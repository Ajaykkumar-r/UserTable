import React from 'react';
import ReactDOM from 'react-dom';
import users from '/home/ubuntu/Desktop/table/src/store.js';
require('/home/ubuntu/Desktop/table/src/index.css');

class Table extends React.Component {
    render() {
        return (
            <table>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Dob</th>
                    <th>Age</th>
                    <th>Address</th>
                    <th>PhoneNumber</th>
                </tr>
                {users.map((user) => {
                    return <tr>
                                <td>{user.id}</td>
                                <td>{user.name}</td>
                                <td>{user.dob}</td>
                                <td>{user.age}</td>
                                <td>{user.address}</td>
                                <td>{user.phoneNumber}</td>
                           </tr>
                })}
            </table>
        );
    }
}


ReactDOM.render(
    <Table/>, 
    document.getElementById("root")
);